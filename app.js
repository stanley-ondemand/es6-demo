'use strict';

var koa = require('koa');
var path = require('path');
var favicon = require('koa-favicon');
var logger = require('koa-logger')
var cookieParser = require('cookie-parser');
var bodyParser = require('koa-bodyparser');
var serve = require('koa-static');
var route = require('koa-route');
var cors = require('koa-cors');
var coViews = require('co-views');

var index = require('./server/routes/index');
var reveal = require('./server/routes/reveal');
//var connection = require('./server/tunnel')().init();

var app = koa();
 
app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser());
app.use(cors());

app.use(serve('./'));

//routes
app.use(route.get('/', index));
app.use(route.get('/reveal', reveal));

// catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   var err = new Error('Not Found');
//   err.status = 404;
//   next(err);
// }); 

app.use(function *(){ 
  this.throw('Not Found', 404)
});

// error handlers

// development error handler
// will print stacktrace
if (app.env=== 'development') {
  //app.locals.pretty = true;
  app.use(function *() {
    this.res.status(err.status || 500);
    this.res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// connection.init().query('SELECT * FROM sg_store',(err,rows) => {
//   if(err) throw err;
 
//   console.log('Data received from Db:\n');
//   console.log(rows);
// });

module.exports = app;
