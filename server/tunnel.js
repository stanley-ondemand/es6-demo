var mysql = require('mysql');
var tunnel = require('tunnel-ssh');

module.exports = function (server) {

    return new Object({
 
            /**
             * Default configuration for the SSH tunnel
             */
            tunnelConfig: {  
                //host: '172.16.0.8',
                //username: 'fc24',
                //dstPort: 3306, 
                dstPort: 33306, 
                host: '192.168.33.20',
                port: 22,
                username: 'sg'
                //privateKey: require('fs').readFileSync('')
            },

            /**
             * Initialise the mysql connection via the tunnel. Once it is created call back the caller
             *
             * @param callback
             */
            init: function (callback) {
                //
                // SSH tunnel creation
                //
                var server = tunnel(this.tunnelConfig, function (error, result) {
                     console.log('Tunnel connected', error);
                    //
                    // Connect to the db
                    //
                      var conn = new mysql.createConnection({
                          host: 'localhost',
                          user: 'root',
                          password: '',
                          database: 'marketplace' 
                        });

                        conn.query('SELECT * FROM sg_store',(err,rows) => {
                          if(err) throw err;
                         
                          console.log('Data received from Db:\n');
                          console.log(rows);
                        });

                        conn.end(function (err) {
                            console.log("MYSQL::END");
                        });
                }); 
            },

            /**
             * Mysql connection error handling
             *
             * @param err
             */
            errorHandler: function (err) {

                var me = this;
                //
                // Check for lost connection and try to reconnect
                //
                if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                    console.log('MySQL connection lost. Reconnecting.');
                    me.connection = me.connect();
                } else if (err.code === 'ECONNREFUSED') {
                    //
                    // If connection refused then keep trying to reconnect every 3 seconds
                    //
                    console.log('MySQL connection refused. Trying soon again. ' + err);
                    setTimeout(function () {
                        me.connection = me.connect();
                    }, 3000);
                }
            },

            /**
             * Connect to the mysql server with retry in every 3 seconds if connection fails by any reason
             *
             * @param callback
             * @returns {*} created mysql connection
             */
            connect: function (callback) {

                var me = this;
                //
                // Create the mysql connection object
                //
                var connection = mysql.createConnection(me.dbServer);
                connection.on('error', me.errorHandler);
                //
                // Try connecting
                //
                connection.connect(function (err) {
                    if (err) throw err;
                    console.log('Mysql connected as id ' + connection.threadId);
                    if (callback) callback();
                });

                return connection;
            }
        }
    );

};