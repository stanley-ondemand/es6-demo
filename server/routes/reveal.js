
var render = require('../../views');

module.exports = function *(){
  var data =  { title: 'ECMAScript 2015 </br> --- aka --- </br> ES6 </br> --aka-- </br> ES Harmony' };
  this; // is the Context
  this.request; // is a koa Request
  this.response; // is a koa Response

  try{
    var html = yield [render('reveal.jade', data)];
    this.body = html.join(''); 

   }catch (err) {
   
    this.throw(500,  err.message) 
  }
};
