// var express = require('express');
// var router = express.Router();

// /* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'SG Front-end' });
// });

//module.export =  router


var render = require('../../views');

module.exports = function *(){
  this; // is the Context
  this.request; // is a koa Request
  this.response; // is a koa Response

  try{
    var html = yield [render('index.jade',  { title: 'SG Front-end' })]

    this.body = html.join(''); 

  }catch (err) {
   
    this.throw(500, err.message) 
  }

};
