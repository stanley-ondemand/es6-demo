
var gulp = require('gulp'); 
var browserify = require('browserify');
var taskListing = require('gulp-task-listing');
var babel =  require('gulp-babel');
var jade = require('gulp-jade');
var express = require('express'); 
var nodemon = require('gulp-nodemon');
var source = require('vinyl-source-stream');
var rename = require('gulp-rename');
var babelify = require("babelify"); 

var resourcePaths =  {
		scripts: 'public/javascripts/**/*.js', 
		dist: 'dist/'
};

gulp.task('help', taskListing);

gulp.task('css', ()=> { 
  return gulp.src('public/images/**/*')
    .pipe( gulp.dest('dist/assets/images/'))
});
 
gulp.task('images', ()=> { 
  return gulp.src('public/stylesheets/**/*.css')
    .pipe( gulp.dest('dist/assets/stylesheets/'))
});
 
gulp.task('normal-libs', ()=> { 
     return gulp.src('public/javascripts/lib/head.min.js')
    .pipe( gulp.dest('dist/assets/'))
});
 
 // Browserify npm packages
gulp.task('browserify-lib', ()=> {
  // create a browserify bundle
  var brwsfiy = browserify();

  // make reveal available from outside node module lookup 
  brwsfiy.require('reveal');

  // start bundling
  return brwsfiy.bundle()
    .pipe(source('reveal.js'))   // the output file is reveal.js
    .pipe(gulp.dest('./dist/assets'))
})

var files = [
  './public/javascripts/es6presento.js'
];

gulp.task('js', ()=> {
     
 console.log('js has ran'.repeat(3))
 
 try{
   return browserify(files)
     .transform(babelify)
     .external(require.resolve('reveal', {expose: 'Reveal'})) 
     .bundle()
     .pipe(source('main.js'))    // the output file is main.js
     .pipe(gulp.dest('./dist/assets'))
 }catch(e){
   console.log(`I made ${e} blow up on purpose`)
 }
});

gulp.task('templates', ()=> {
 	return gulp.src('views/*.jade')
    .pipe( gulp.dest('dist/views'))
})
 
gulp.task('serve', ()=> {
 	nodemon({
    script: 'server/server.js',
	  //ext: 'js html',
		env: { 'NODE_ENV': 'development' }
  })
})

gulp.task('default', ['js', 'normal-libs' ,'css', 'images', 'templates'])