"use strict";

import Reveal from 'Reveal';
import Examples from './defaultRestSpread.js';
import { doublePound } from './maps';
import { toggleGif, msgAfterTimeout } from './promiseMe';

 class Presentation {
   constructor(slides, name='Temp Name'){
     this.slides = slides || 1;
     this.name = name;
     
      // template string variable bindings / interporlation
      var name = "right", 
          time = 1,
          array = ['ES6','ES5','ES3'],
          sym = Symbol("Test");
         
      console.log(element); //will equal undefiend because of hoisting
      console.log(sym);
      
      try{
      //of course I'll error here 
      console.log(elementJuice);
      }catch(e){
        console.log(e);
      }
      
      for (let index = 0; index < array.length; index++) {
        var element = array[index];
        let elementJuice = 'None';
      }

     setTimeout(() => {
      toggleGif() 
     }, 5000)
     
     setInterval(() => {
        this.slides++; 
        
      }, 1000)
      
   }
   
   static flash() {
      console.log(this);
   }

   // get name() {
   //   return this.name.toUpperCase();
   // }

   // set name(newName){
   //    if(newName){ 
   //        this.name = newName;
   //    }
   //  }

   initProject() {
     /*
      * init reveal instance so we can USE IT
      */
      Reveal.initialize({
        controls: true,
        progress: true,
        history: true,
        center: false,
        slideNumber: true,
        transition: 'convex',
         dependencies: [
        	  { src: 'https://rawgit.com/hakimel/reveal.js/master/plugin/highlight/highlight.js', 
              async: true, 
              callback: ()=> {
                hljs.initHighlightingOnLoad(); 
               } 
            }
          ]
       })

      Reveal.addEventListener( 'slidechanged', (event) => {
        if(event.currentSlide.id !== 'titleSlide'){
          toggleGif(false);
        }else{
          toggleGif(true);
        }

      })
   }
    
   /*
    * Allow code blocks in presentation to be toggled for editing
    */  
   makeContentEditable(isEditable = true) {
      //array of html elements
      var codeBlocks =  document.querySelectorAll('code');
      let setAttr = function(isedt){
        for(let block in codeBlocks){
          let blck = codeBlocks[block];

          if(typeof blck === 'object' )
            !isedt ? blck.removeAttribute('contenteditable') : blck.setAttribute('contenteditable', '');
       }
      }

      isEditable ?  setAttr(true) : setAttr(false);
   }
 }

let presentation = new Presentation(12, 'Harmony');

presentation.initProject();

//Default param will make this false
presentation.makeContentEditable();

Examples.setupSlides();

console.log(presentation);

msgAfterTimeout("", "Foo", 100).then((msg) =>
    msgAfterTimeout(msg, "Bar", 200)
).then((msg) => {
    console.log(`done after 300ms:${msg}`)
})
//console.log(presentation.getName());

//console.log(new Presentation(12));
//console.log(new Presentational().blast());