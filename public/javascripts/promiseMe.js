
function msgAfterTimeout (msg, who, timeout) {
  return new Promise((resolve, reject) => {
      setTimeout(() => resolve(`${msg} Hello ${who}!`), timeout)
  })
}

var toggleGif = (toggle=true) => {

  var el = document.getElementById('homeGif')
 
  if(toggle){
    el.setAttribute('style','background-size: cover; background-image: url(http://i.imgur.com/RtFKusK.gif)');    
  }else{
    el.setAttribute('style','');
  }
}

export { toggleGif, msgAfterTimeout }